package org.vitalii;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

/**
 * Unit test for SortingApp.
 */
@RunWith(Parameterized.class)
public class AppTest extends TestCase {

    private final String[] input;
    private final String expected;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    public AppTest(String[] input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }


    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                { new String[0],
                        "Invalid input! You should enter more then 1 and up to 10 numbers."},
                { new String[]{"1"},
                        "[1]"},
                { new String[]{"1", "5", "3", "7", "4", "59", "34", "54", "2", "8"},
                        "[1, 2, 3, 4, 5, 7, 8, 34, 54, 59]"},
                { new String[]{"1", "5", "3", "7", "4", "59", "34", "54", "2", "8", "9"},
                        "Invalid input! You should enter more then 1 and up to 10 numbers."},
        });
    }

    @Test
    public void testCornerCases() {
        App.main(input);
        assertEquals(expected, outputStreamCaptor.toString().trim());
    }

    @After
    public void tearDown() {
        System.setOut(System.out);
    }

}
