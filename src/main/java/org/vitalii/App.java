package org.vitalii;

import java.util.Arrays;

/**
 * The program gets up to 10 integers and prints them in sorted way
 *
 */
public class App
{
    /**
     * Main and only method of a program
     * @param args should be provided as command-line arguments
     *
     */
    public static void main( String[] args )
    {
        if(args.length > 10 || args.length < 1) {
            System.out.println("Invalid input! You should enter more then 1 and up to 10 numbers.");
            return;
        }
        int[] numbers = new int[args.length];
        for(int i = 0; i < args.length; i++) {
            numbers[i] = Integer.parseInt(args[i]);
        }
        Arrays.sort(numbers);
        System.out.println(Arrays.toString(numbers));
    }
}
